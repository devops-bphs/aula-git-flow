# aula-git-flow

Pare de começar e começe a terminar!

# Comandos aprendidos neste aula, gitflow 



# Cria as branches uma a uma seguindo o modelo gitflow 

git flow init 

# Lista as branches existentes 

git branch --list 

# Criando uma branch feature 

git flow feature start criar_feature

# Após a criaçãoserá informado que foi criado uma feature a partir da branche develop, e que após de terminar o desenvolvimento a mesma deverá ser encerrada com o comando abaixo: 

git flow finish criar_feature 

# Trabalhando com as branches do git flow 

# git flow feature (start/publish/pull/finish) crir_menu [criada a partir da develop, após o término a mesma será finalizada e excluída fazendo o merge da develop] 

# git flow release (start/publish/pull/finish) 1.0 [criada a partir da develop, após o término a mesma será finalizada e excluída, fazendo o merge na main] 

# git flow hotfix (start/publish/pull/finish) bug_cadastro [criada a partir da main, após o término a mesma será finalizada e excluída, fazendo o merge na main] 



